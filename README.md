# LukFPV
Repository dedicated to the Computational Biophysics project

This file contains the description of the project folder. First of all, all the folders do have their 'readme.md' file, that explains locally their organization (if not obvious and immediate). 
As you might note, the structure is similar to the one proposed by you; however, some directory could be absent and other could have a slightly different name.

Overview of the structure:

- data -> contains all the .pdb/.gro/.xtc [..] files used to obtain the final trajectory of the protein
- LaTex -> contains the source code of our review
- notebooks -> contains our jupyter-notebooks
- references -> contains useful paper about LukF-PV
- report -> contains files and images used in the production of our report (output of the notebooks)

The other files or folders are created by git and do not have particular meaning to us
NB: the .gitignore avoid uploading folders with huge data (both of us executed them locally)
