#!/bin/bash
#PBS -l walltime=005:50:50
#PBS -l select=1:ncpus=8
#PBS -e localhost:/home/nicholas.tovazzi/LukFPV/notebooks
#PBS -o localhost:/home/nicholas.tovazzi/LukFPV/notebooks
#PBS -q short_cpuQ
#PBS -N rmsd_map_short


module load miniconda

cd $PBS_O_WORKDIR
source activate QCB

python3 rmsd_map.py
