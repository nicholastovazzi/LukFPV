# Importo tutto, nel dubbio:
import numpy as np
from pathlib import Path
from os import fspath
import math
import pickle
from pathlib import Path
from os import fspath
import MDAnalysis as mda
from MDAnalysis.analysis.rms import RMSD, rmsd
import time

setup_path = Path('../data/03_prod_out/')
GRO = setup_path / 'step7_1.gro'
XTC = setup_path /'step7_1.xtc'

u = mda.Universe(str(GRO), str(XTC),in_memory = True, in_memory_step =  5)
u_new = u.copy()

start = time.time()
rmsd_map = [rmsd(u.select_atoms('name CA').positions, u_new.select_atoms('name CA').positions, center=True, superposition=True) for ts in u.trajectory[:] for ts_sas in u_new.trajectory[:]]
end = time.time()
print(end-start)

with open("rmsd_map","wb") as fp:
    pickle.dump(rmsd_map,fp)
