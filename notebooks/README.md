# Jupyter-notebooks

## Luke-SkyWalker
Our analysis is all made by the notebook Luke_SkyWalker. We decided to condensate all our functions inside it and it contains calculations and data unused in the final review, which should be taken as "attempts" or double confirm of our results.

## Luke-Extended
This analysis serves to evaluate briefly the extended trajectory: the output are different from the previous and, in order to save time, we set all of the in the data directory "extended_production"

## Obi-Wan-Kenobi
This notebook serves as a pyfferaph evaluator: we used a different notebook in order to avoid kernel problems. Unless it works properly, we chose not to use it, since its usage does not strenghten our analysis about the protein-membrane system. 

## rmsd_map
These are two files used on the cluster in order to save time in the rmsd_map production. Thanks to pickle we then reimport locally the results.
