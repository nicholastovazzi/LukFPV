\chapter{Materials and Methods}

We started from a downloaded Protein Data Bank (PDB) file (code 1PVL) containing a three-dimensional X-ray structure of the secreted, water-soluble form of LukF-PV, resolved at 2.0 Å resolution \cite{[8]}. This PDB file contains 301 of the total 325 residues of the protein, from residue 25 to 325 (verified on the UniProt(KB) website, protein code: Q5FBD2), as the 1-24 residues belong to the signal peptide region. The PDB file was already `clean', i.e. contained only the protein without external ligands useful for e.g. crystallisation procedures. 

\section{CHARMM-GUI system generation pipeline}
To implement the system, we utilized the `Membrane builder' module from the CHARMM-GUI website, an online tool that allows to create a huge variety of systems, such as a membrane-protein one. CHARMM-GUI implements a specific pipeline, composed of seven different steps:

\begin{itemize}
\item \textbf{Read PDB coordinates}: 1PVL PDB file was passed as input to the software; the orientation used was the one retained by the PDB file; no disulfide bond was selected, as LukF-PV does not have any. 

\item \textbf{Protein-membrane orientation}: the option `Align the First Principal Axis Along Z box' was selected, and the cross-sectional area plot was checked; the system should feasible, with only a small protein loop inserted into the membrane. 

\item \textbf{Lipid composition of the membrane}: a two-components lipid bilayer was used; specifically, phosphatidylcholine (POPC) and Cholesterol in ratio 7:3. The bilayer was chosen symmetric (the outer and inner layers have the same composition). Lastly, a system size along X and Y of 100 angstrom was selected (some warnings could harmlessly emerge).

\item \textbf{Build components}: the temperature, concentration chosen were to 310K, 0.15 M, respectively. The species of added ions was KCl, and the replacement methodology was solvent molecules (water). 

\item \textbf{Assemble components}: this part checks the system stability of each system component, preventing the generation of unstable and unfeasible simulations. The results from the control tests were encouraging, as both `Protein surface penetration' and `Lipid ring penetration' checks were successfully passed. No unphysical artifacts such as water molecules into the membrane hole generated to insert the protein or lateral chains of residues into structural aromatic rings of the lipid molecules were found. At the end of this step, a final PDB file is generated. This file is the one that will be used for our analysis. 

\item \textbf{Energy minimization and equilibration}: this is the last phase of the CHARMM-GUI pipeline. The software generated seven files (.mdp format, since `GROMACS' option was chosen) for energy minization and equilibration phases. The first .mdp file was used for the energy minimization; the integrator used for the minimization was a steepest descent algorithm with position restraints on heavy atoms of backbone residues with force tolerance equal to 1000 kJ/(mol·nm\textsuperscript{2}). The remaining six files were sequentially used to equilibrate our system; therefore, the equilibration phase was split into six different runs. Initially a short NVT equilibration was suggested from the CHARMM-GUI software (the second .mdp file used the Berendsen thermostat, with no coupling on the pressure), followed by an NPT equilibration (the last five .mdp files all use the Berendsen thermostat for both temperature and pressure coupling). As reminder, temperature chosen was 310 K, the pressure 1 atm (circa equal to 1 bar) to mimic the cell conditions. 
Note that CHARMM-GUI only outputs the input .mdp files needed for both minimization and equilibration, performed subsequently by us with the GROMACS tool (2018 version). 
\end{itemize}

Whether the minimization was useful to remove steric clashes and minimize potential energy (move our structure to a more relaxed one), we would like to motivate the choice taken by CHARMM-GUI software of performing NVT followed by NPT equilibration: NVT equilibration is essential to let the system -membrane and protein- find a proper organization/orientation at constant volume, whereas NPT is crucial mostly for the membrane to adjust its density and lipid re-arrangements. The insertion of the protein made by the software was a rough process, where membrane molecules were removed from the file to free space for the protein atoms. Therefore, before passing to the production run we would like to verify that our system properly reached a state close to the real equilibrated one, at least without huge artifacts. This section will be better elucidated in the section `Results'. 

Lastly, in the .mdp files that CHARMM-GUI gave us, all the .tpr files for the energy minimization and equilibration runs were generated with the GROMACS module `grompp', with standard inputs like the structure file, .mdp file, topology file. 



\section{Production run}
The production run was performed with the GROMACS tool (2018 version) on the HPC cluster of the University of Trento. The pipeline of commands followed is a classical one. The .mdp file that the CHARMM-GUI software provided us, was used according to this new set of parameters: no constraints on heavy atoms anymore; weak coupling Berendsen thermostat is substituted by Nosé-Hoover temperature coupling, which correctly samples the canonical ensemble; weak coupling Berendsen barostat is replaced by Parrinello-Rhaman pressure coupling, which correctly samples the isothermal-isobaric ensemble. We performed an NPT production run. 

Moreover, some parameters in the same .mdp file were changed: the number of total simulation steps was increased to 300 [ns], so to 1.5·10\textsuperscript{8} units, given that a single time step consists of 2 femtoseconds [fs]. The parameters `nstxout', `nstvout' and `nstfout', containing the frequencies at which the .trr file is written, were modified to 5·10\textsuperscript{5} steps. Therefore, the .trr file will be written every 5·10\textsuperscript{5} · 2·10\textsuperscript{-3} = 10 \textsuperscript{3} [ps], so every 1 [ns]. This was done because if written too frequently, the .trr file results to be too heavy. Lastly, the flag `nstxout-compressed' was set to 2.5·10\textsuperscript{4}. In this way, the .xtc file will be written every 2.5·10\textsuperscript{4} · 2·10\textsuperscript{-3} = 50 [ps], so 0.05 [ns]. Given a total simulation time of 300 [ns], and a pace of 0.05 [ns], our final trajectory consists of 6000 frames. \newline \newline

\noindent\textbf{General note}: all the mentioned files (.mdp, .tpr, .pdb, .xtc, ..., all the launchers on the HPC with all the GROMACS commands) can be found in the compressed folder shared on the Drive of the course. Inside it there is a README.txt file in which every folder path/content is properly explained, to facilitate any inspection. Please, use it if needed.


\section{Trajectory manipulation and analysis}

\subsection{Removal of PBC and centering settings}

In this brief section we describe how we treated our trajectory once downloaded from the cluster, before analysing it. Our concerns were focused on removing the periodic boundary conditions (PBC) and performing a correct centering and alignment on the protein, in order to avoid issues on downstream analyses. We used the `trjconv' module of GROMACS in a two-step fashion: first we generated a new trajectory selecting for the -pbc parameter to `nojump' as type of periodic boundary condition treatment; secondly, we gave this newly generated trajectory as input to a subsequent run of the same module, with the -fit parameter set to `rot+trans', to fit molecule to reference structure in the structure file (the .tpr file in this case). We adopted these settings because out of all our trials, these gave the best results. A sketch of the two commands is reported here below:

\begin{itemize}
\item \textbf{gmx trjconv -s .tpr -f .xtc -pbc nojump -o .xtc}

\item \textbf{gmx trjconv -s .tpr -f .xtc -fit rot+trans -o .xtc}
\end{itemize}



\subsection{Tools and parameters settings}

In this paragraph it will be briefly explained how the results were generated. The tools used for the analysis were mainly three: a python package called MDAnalysis, Pyfferaph and lastly GROMACS.\newline  

\noindent\textbf{RMSD and RMSF:} the two quantities were measured with both GROMACS and MDAnalysis. Both quantities were performed on the C-$\alpha$ atoms. The package uses the following formulae:

$$RMSD \ (r_1,r_2) = \sqrt{\frac{1}{N}\sum_{i=1}^{N} \delta_i^2} $$

\noindent{where} the indices $i$ run over the subset of selected atoms and $\delta_i$ is the deviation of atom $i$ in frame $r_2$ with respect to the position of atom $i$ in the reference frame $r_1$:

$$\delta_i^2 = |\vec{x}_{(2)i} - \vec{x}_{(1)i}|^2.$$

\noindent{For RMSF}, the following formula was used (the notation is the same):

$$RMSF_i = \sqrt{\frac{1}{T}\sum_{t_j=1}^T|\mathbf{r}_i(t_j)-\mathbf{r}_i|^2}$$

\noindent{where} the indices $i$ run over the subset of selected atoms.\newline 

\noindent\textbf{Block analysis:} this analysis was performed since measuring the error on the average is difficult because the formula: 
$$ s(\bar{X}) = \sqrt{\frac{\textrm{Var} (X)}{N}} $$
considers $N$ independent measurements of the random variable $X$. However, in time series independence is not trivial to obtain (an auto-correlation analysis should be performed, in order to find the autocorrelation time). Block Analysis was then used to retrieve a reliable estimate of $s(\bar{X})$ as well as a way to estimate when the trajectory has been properly equilibrated (once reached the equilibration, all the frames generated downstream will depend only on the advancing time, not on the one needed to arrive to an equilibrated state). 

To estimate then the standard error on the average:
\begin{enumerate}
\item {Normally, we should select a set of chunk sizes, $m \in {2,3,...,N_{samples}/2}$ to divide our trajectory into. We decided to use only divisors of the total number of frames as values for the number of generated blocks per iteration, to avoid having too big variances, especially when block lengths are high (that means, the number of blocks becomes low and by consequence the relative error oscillates a lot). }
\item{For each $m$, divide the trajectory in $N_c^{(m)}$ smaller chunks of size $m$, with $N_c^{(m)} = N_{samples}/m$}
\item{For each of those, compute the mean of the random variable $X$, $\bar{X}_i^{(m)}$}
\item{For each value of $m$, compute the standard deviation of  $\bar{X}_i^{(m)}$, and divide it by $N_c^{(m)}$. In other words, compute the error on the total average, according to each iteration.}
\item{Save the results as a function of $m$, and plot them.}
\end{enumerate}
These results will be used later on in order to spare time in the clustering analysis, without affecting too much our capability to represent the system. \newline

\noindent\textbf{PCA analysis:} in many cases it is impossible to distinguish \emph{a priori} the independent components of a system made by a multitude of atoms in time. Hence, we compute a fitting procedure with the aim of reconstruct an orthogonal basis of components on which the system can be represented. Briefly: the aim is to look for uncorrelated quantities that represent the main features of the system (i.e. acceleration along an axis or angles of rotation). After having found an orthogonal system, for each vector is computed a variance. Then, we sort the vector from the biggest to the lowest variance and keep only the first n components, chosen in order to have their cumulative variance above a certain threshold of accordance. 
In our case, only the first three components are needed, allowing us to represent the subspace with a 3D plot. In this case, by looking at the plot we have a deep insight of the behaviour of the system: since each point of the trajectory represents the system in a reduced basis, it shows how the system evolves in time.  
This analysis was performed with the MDAnalysis package on the coordinates of the C-$\alpha$ atoms of an aligned trajectory (the alignment was performed with the `Aligntraj' module of MDAnalysis package). Plots were generated with Matplotlib and Plot.ly Python libraries.\newline 

%The entropy formula used was: IO QUESTA NON LA METTEREI ONESTAMENTE, NON MI SEMBRA UTILE
%$$S = - \sum_{i} p_i \ log_2(p_i) $$ 

\noindent\textbf{Clustering analysis:} the clustering was performed with the on an all vs all RMSD matrix performed on only a subset of the total frames, 200 (1 frame per 30 frames was taken from the initial trajectory of 6000 frames) since a 6000 vs 6000 matrix was too computationally heavy (more details in the section `Results'). The linkage modality chosen was `average linkage', the criterion used `distance'. It discerns the various states of the system by its RMSD (previously discussed). By producing a heat-map we have a visual support of our consideration on the RMSD, furthermore, it is possible to see how the various states are distributed: if the system spends multiple subsequent frames in the same state it is possible that it represent a specific state of the system. \newline 

\noindent\textbf{Density plots:} 
Density plots were generated with GROMACS in different steps
\begin{enumerate}
    \item generate an index with the needed groups \textbf{gmx make\_ndx -f .tpr -o .ndx}
    \item select a group and save the output as an .xvg \textbf{gmx density -f .gro -s .tpr -n .ndx -o .xvg } \newline We chosen not to symmetrize it, but to keep the representation from from 0 to zmax
    \item repeat the previous step for all the groups needed
    \item Compute the density change in time \textbf{gmx energy -f .edr -o .xvg}
    \item import the .xvg with a import function in python and plot them with Matplotlib
\end{enumerate}

\noindent\textbf{Protein-membrane distance analysis:} 
This analysis was performed with GROMACS tools able to evaluate distances, such as the \emph{mindist} module, and vmd support. The steps are similar to the density analysis
\begin{enumerate}
    \item create a index file with the command above
    \item with the command \textbf{gmx mindist -f .xtc -s .tpr -n .ndx -od .xvg -tu ns} select two group between which evaluate the minimum distance in time (one should be the POPC group) 
    \item use vmd to extract different frames of the trajectory to evaluate and save them as .pbd files
    \item compute the minimum distance per residue of the protein-membrane system in a particular frame by using \textbf{gmx mindist -f .pdb -s .tpr -n .ndx -os .xvg}
    \item import the .xvg with a import function in python and plot them with Matplotlib
\end{enumerate}
This analysis allow us to see the global minimum distances of all residues and the membrane at any time and understand how the protein is moving. Furthermore, it is possible to look at residue in opposite position of the protein in order to see at which side it is going to bend.\newline


\noindent\textbf{General note}: more details about the script (parameters, used functions, \dots) can be found in the shared folder, in the `notebooks' section (check the README file inside the folder).