\chapter{Results}

\section{Structure of PBD generated file}
In figure \ref{Figure_4_1} can be seen the system that the CHARMM-GUI software generated. Therefore, this system underwent minimization, equilibration and then production run, according to the modalities cited in the section `3. Materials and methods'. Hence, it is possible to see that the generation of the system has gone for the best, as the protein is correctly placed in the membrane. However, from this image it is quite evident that water molecules present some artifacts that should be removed before going through the production run. We want to mention that residues between THR187 and LEU201, and some residues between GLN258 and TRP261, are the ones that CHARMM-GUI inserted in the membrane; this aspect will be important for further analyses at the end of our report.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.45\textwidth]{imgs/fig_4.1A.png}
    \includegraphics[width=0.45\textwidth]{imgs/fig_4.1B.png}
    \caption{A VMD snapshot of the .gro file generated by CHARMM-GUI prior to minimization, equilibration and production.}
    \label{Figure_4_1}
\end{figure}


\section{Equilibration of the system}

Therefore, this paragraph will have the aim to show that our system has properly equilibrated during the equilibration phase and the beginning of the production run. 

First, we want to report the last frame of our trajectory, shown in figure \ref{Figure_4_2}. As we can see from this snapshot, the protein changed its orientation and seems to bend towards the membrane (this aspect will be expanded in the section `4.4 Protein-membrane interactions'). 

\begin{figure}[h]
    \centering
    \includegraphics[width=0.45\textwidth]{imgs/fig_4.2B.png}
    \includegraphics[width=0.45\textwidth]{imgs/fig_4.2A.png}
    \caption{A VMD snapshot of the last frame of our trajectory. It is possible to see that the protein has changed conformation from the previous representation, and that it is devoid of water artifacts.}
    \label{Figure_4_2} 
\end{figure}

In order to see what happened between the pre-production and the last frame we have to look into all the parameters able to represent the state of the system. At first it is better to see how much time does it take during the production run to minimize properly. We expect that the system (surely deprived of artifacts and constraints) is going to reach a first meta-stable state in a little time. How can we see it?   


\subsection{PCA analysis}
The PCA in figure \ref{Figure_4_3} is the first proof that we report to indicate that our system has properly reached its equilibration state. In fact, it is possible to observe a gradient of colour that goes from the first frames (violet) to the last ones (yellow), typical pattern for a PCA of a trajectory undergoing equilibration phases. Moreover, we can distinguish two main groups, one smaller composed by the initial frames (0-800, equivalent to c.a 40 ns) and another quite bigger and more heterogeneous (800-6000); therefore, probably the protein started from a configuration that was inherited from the equilibration phase and then, as the number of frames increased, re-arranged its position in the space, reaching and retaining the same state for basically the whole trajectory (from 800-6000). This can be evinced from the fact that the points in the second bigger group tend to stay quite close to each other, without detaching too much from each other. In other words, the high degree of heterogeneity of this second group (800-6000) should prove that our protein completely achieved an equilibration state. Even in the cluster analysis will be reported a similar pattern, in which there is a strong separation between two groups, one quite small and the other bigger, displaying a more heterogeneous composition. 

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.75\linewidth]{imgs/fig_4.3A.png}
    \includegraphics[width=0.12\linewidth]{imgs/fig_4.3B.png}
    \caption{PCA plot: the first three components explicate together more than 90\% of the total variance. The plot was done on the C-$\alpha$ atoms of the trajectory. Purple points represent the first frames, yellow points the last frames.}
    \label{Figure_4_3} 
\end{figure}

\subsection{RMSD and RMSF}
After the PCA, our intent was still to verify the equilibration of the system. To do so, we decided to perform an RMSD inspection, by setting as reference frame the initial frame of the simulation. 

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{imgs/fig_4.4A.png}
    \includegraphics[width=0.8\textwidth]{imgs/fig_4.4C.png}
    \caption{RMSD, RMSF plots: these plots were generated both with MDAnalysis and GROMACS, and, as seen from the RMSF graph, both programs displayed almost the same results. Details about the two measurements are present in the section `3. Materials and methods.'}
    \label{Figure_4_4}
\end{figure}

As reported from figure \ref{Figure_4_4}, the RMSD seems to stabilize around values between 2 and 2.5 Å, indicating that the system could have reached an equilibrated state. However, we still wanted to understand the behaviour of its shape, as we expected to see a more stable curve. Therefore, an RMSF analysis was conducted, to measure the degree of fluctuation of every residue (figure \ref{Figure_4_4}). Our hope was to find short parts of the protein chain exhibiting high fluctuations, biasing the overall RMSD values. This suspect was partially confirmed: the last residues composing the C-terminus do move quite significantly, probably because are part of a highly unstructured region like a terminus of a protein. In fact, if our protein had even the N-terminus (signal peptide region, 1-25 aminoacid cut away from the PDB file creators \cite{[8]}), we would have seen probably the same behaviour in the first residues values of RMSF. This RMSD is still however a bit enigmatic: all we can say is that our alignment procedure went positively, and the protein loaded in VMD seemed to have a decent alignment, without displaying any movement around the space, not leaving its centred position. Overall, it is still a reliable result, in concordance with the PCA regarding the aspect of the system equilibration. 

Regarding the RMSF, we can see that there are two main peaks displaying high fluctuation around residues 120-150. As reported in the section 1.2 of the Introduction chapter, this protein has two main cores of $\beta$-sheets, the $\beta$-sandwitch and the rim domain, quite conserved along whole protein family. These two $\beta$-strands are connected by loops, which tend to move heavily if compared with a rigid structure element as a $\beta$-sheet. By a qualitative inspection with VMD, it was possible to assign a partial responsibility for these high fluctuations to all the residues forming these loops, connecting the $\beta$ strand cores. Moreover, we tried to verify if some residue belonging to the the stem domain region (responsible for the formation of the pore, together with LukS-PV monomers, 110-130 residue numbers in the chain) displayed some fluctuation. Unfortunately, that domain does not show any particular sign of movement, indicating that our protein needs the interaction with LukS-PV to start conformational rearrangements leading to the pore formation. Lastly, it is important to mention that the magnitudes of both RMSD and RMSF are quite comparable, retaining values around 1.5-2.5 Å, on average. 


\subsection{Block analysis and clustering}

From the previous sections we learned that the protein is in a stable state. However, this is merely a supposition: it is legit to assume it from the plots, but no quantitative approaches were made in order to show it. We know that the RMSD is a quantification of the system configuration, and given that we determined it at each time instance, we can use it in order to highlight if at two different time steps the system is found in a similar configuration. At this point we can say that each point with similar RMSD can be grouped in a cluster, thus reducing the complexity of the system by far. In order to do it we need a bi-dimensional RMSD (or `all vs all' RSMD), which represents for our standards laptop a problem, since our total trajectory is 6000 frames long: with a good CPU the time taken for computing a single frame RMSD is 25 [ms] (evaluated by computation). Hence, the total time needed is $$ 0.025 s \cdot \frac{6000\cdot 6000}{3600s/h\cdot 240} = 10 \text{ days}$$     
Since it is not a multi-threaded process, we cannot in principle assign it to multiple cores and reduce the time taken, even if we use the cluster (since the power of cluster's single CPUs is similar to ours). \\ What we can do is to implement a so called Block Analysis, and see how many frames are truly needed to represent the motion of the system without losing too much information, that means, looking for independence of the frames. In other words, find a cut-off that ensures two frames are independent. 

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\textwidth]{imgs/fig_4.6.png}
    \caption{Block analysis results; details about this technique and modalities in the `3. Materials and methods' section.}
    \label{Figure_4_5}
\end{figure}


By looking at the figure \ref{Figure_4_5} it is possible to see that if we take a value in between [30,200] block we are on the function bow: for any value bigger than 200 the result vary a little. This consideration allow us to use 200 frames for the clustering, without incur in a wrong representation of the system and thanks to this simplification we can afford to compute the rmsd\_map in just half an hour.

We built the RMSD map with our subset of frames, arriving with a 200x200 matrix (figure \ref{Figure_4_6}). This matrix represented the basis for our clustering analysis. Qualitatively, this heatmap allows to distinguish two main regions: one identified by the blue colour, mainly referred to the first frames, and a larger one, that accumulates the majority of the frames. Even though this analysis was done on a subset of the total pool of points with which all the other analyses were performed, we can still see the same pattern verified in the PCA. In fact, the division between the two groups occurs quite early, meaning that probably our system reaches fast a sort of principal state and oscillates around it for the whole simulation. By logic, we expect to find the same pattern even in the hierarchical clustering application.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.70\textwidth]{imgs/fig_4.5.png}
    \caption{RMSD map, computed on only 200 frames (1 per 30 frames). Two principal pattern can be distinguished, in concordance with the PCA (even though this is a qualitative comparison, due to 1. the different set of points used, coordinates of C-$\alpha$ atoms and RMSD) and 2. the number of points used.}
    \label{Figure_4_6}
\end{figure}


\subsection{Hierarchical clustering analysis}

In figure \ref{Figure_4_7} we had a confirm of our prediction: here it is quite evident that frames between 1-30 are clustered all together in the green group; contrary, the red group represents the rest of our frames. We decided then to cut the cluster in two principal groups, as the assignments of observations in the second larger red cluster seemed to be quite random: in fact, the algorithm puts randomly together frames without any logic. Moreover, the heights of the clusters are quite irrelevant, meaning that this unsupervised method is highlighting differences that are given by random oscillation within a local minimum of equilibrium that our system has reached.

\begin{figure}[h!]
    \centering
    \includegraphics[width=1\textwidth]{imgs/fig_4.7.png}
    \caption{Clustering analysis. The analysis was performed on a matrix of all vs all RMSD values, of dimension 200x200.}
    \label{Figure_4_7}
\end{figure}


\subsection{Density evaluation}

Our system equilibration consisted of a two-step process: a first NVT equilibration, performed to let the system -membrane and protein- assume a correct orientation at constant volume, was followd by an NPT phase, crucial mostly for the membrane to adjust its density, by varying the volume (masses of course stay fixed). Therefore, we thought that looking at the density during time could have been an ulterior proof to demonstrate the achievement of an equilibrated state. 

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\textwidth]{imgs/fig_4.8A.png}
    \includegraphics[width=0.7\textwidth]{imgs/fig_4.8B.png}
    \caption{Density plots. On the left it is reported the density of POPC, water and protein molecules with respect to the Z axis (vertical) of the system that CHARMM-GUI provided us (shown in figure \ref{Figure_1_1}). On the right, the density evolution in time during the production run.}
    \label{Figure_4_8}
\end{figure}

First of all, we decided to verify the densities right after the creation of the system by the CHARMM-GUI software (shown in figure \ref{Figure_1_1}), before going in the minimization, equilibration and production run. Given that we centred our system on the Z-axis during the CHARMM-GUI procedures, we decided to plot the density values (computed with GROMACS) in function of the Z-axis values. In this case, the 0 [nm] value corresponds to the bottom of our box, the 17.5 [nm] value to the top. From the left graph of figure \ref{Figure_4_8} it can be seen that these values are feasible: the POPC has a high density in the middle of the box, whereas the protein only in the upper part (it was inserted in the upper leaflet). Contrary, water molecules are present both in the upper and lower part of the box, with decrements at the centre of the axis due to the presence of the membrane. This was done to check the system feasibility, as a supplementary control.

Since we are performing an NPT simulation, looking at the density is quite more informative than checking the pressure, given that pressure exhibits many oscillations; density could be more helpful in order to obtain another proof for system equilibration and stability. Therefore, we measured the density variation during the production run: as seen from the right plot of \ref{Figure_4_8}, it is possible to see that our density oscillates locally but retains an average value of 1020 [kg/m\textsuperscript{3}], with associated variance of only 3 [kg/m\textsuperscript{3}]\textsuperscript{2}. 

We can conclude then that our system reached properly equilibration, as all the analyses that we did were quite in concordance, starting from clustering methods like PCA or hierarchical clustering, to RMSD and density evaluations. 


\section{Protein-membrane distance analysis}
This is the last section of our analysis, in which we add some supplementary analysis to our main ones. As mentioned previously and reported in figure \ref{Figure_1_1}, our protein seem to bend on the membrane, towards the last part of our simulation. We decided then to investigate this aspect, two following operations: 1. analysis of the distance between the protein and the membrane, to better elucidate this bending behaviour of the protein; 2. extension of our trajectory, for 80 [ns] of simulation time. This was done because the protein changes its orientation only at the end of simulation, and therefore we thought it was necessary to add some frames. The total resulting trajectory contains then 380 [ns].

\subsection{Residues-membrane minimum distances}
First, let's proceed by looking at the distances between protein and membrane, specifically at the minimum distance between the membrane and the protein residues, to investigate the bending of the protein towards the lipid surface. This analysis allows us to numerically represent the position of each part of the protein with respect to the membrane, thus giving us some information about the changing position of the residues through the simulation. Therefore, figure \ref{Figure_4_9} reports a graph representing the variation of the residues minimum distance during time, with POPC as reference; three curves are reported, corresponding to the begin (frame 50), middle (frame 3000) and end of the simulation (frame 6000). We selected to plot only these three frames as they seemed the most representative points in our simulation. 

\begin{figure}[h]
    \centering
    \includegraphics[width=0.6\textwidth]{imgs/fig_4.9.png}
    \caption{Minimum distance residues-POPC: this command executes the minimum distance by selecting the couple (residue, POPC molecule) that is the closest among all possibilities. Colours are referred to the different frame taken.}
    \label{Figure_4_9}
\end{figure}

In particular, residues between THR187 and LEU201, and some residues between GLN258 and TRP261, the ones that were inserted prior to minimization, equilibration and production run into the membrane by CHARMM-GUI (figure \ref{Figure_1_1}), are found to stay quite anchored to the membrane for the whole not-extended trajectory of 6000 frames. In fact, figure \ref{Figure_4_9} describes quite well this anchored behaviour (their minimum distance with respect to the membrane is in the order of molecular bonds, less than 1 [nm]); however, by looking at the blue curve (frame 6000), other residues residues (mostly residue 1-120) do exhibit a totally different behaviour if compared with the green (frame 3000) and red (frame 50) curve. This can be due to the fact that our protein is bending towards the membrane with only a portion of its surface, the one involving 1-100 residues. In fact, we can visually see a strong difference in terms of orientation between frame 1 (figure \ref{Figure_1_1}) and frame 6000 (figure \ref{Figure_1_2}), noting that at the end of our production run the protein is still anchored to the membrane with the residues described above. 

As ulterior proof of this re-rotation of the protein with respect to the membrane surface, we decided to selected two residues at opposite sides of the protein: ILE122 and ILE277 (in Appendix \ref{A_1} there is a crude VMD representation of the position of these two residues in the protein). 

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.6\textwidth]{imgs/fig_4.10A.png}
    \caption{Minimum distance between residue ILE122 (red) and residue ILE277 (blue) from the POPC (membrane).}
    \label{Figure_4_10}
\end{figure}

Then, we monitored their behaviour in terms of distance with respect to the POPC along the whole trajectory, as shown in figure \ref{Figure_4_10}. If the protein bends, the distance between these residues and the POPC should diminish. 

As expected, both residues display the same behaviour, evincing a tendency in bending towards the POPC in the last frames, since distance is decreasing. In fact, not only the two residues but the whole protein at the end is becoming more and more attracted by the membrane. Therefore, as reported at the beginning of this chapter, we decided to extend our trajectory in order to investigate this strange bending behaviour towards the membrane. 


\subsection{Trajectory extension}

We extended our trajectory for 80 [ns], and then re-performed the same analysis before: we measured the distance of each residue with respect to the POPC membrane, as in figure \ref{Figure_4_12} reports here below: 

\begin{figure}[h!]
    \centering
    \includegraphics[width=1\textwidth]{imgs/min_distance_frames.png}
    \caption{Minimum distance between each residue at frame 3000 (green) and at frame 6800 (black). Arrows represent the evolution in time of the distances of residues between the two frames.}
    \label{Figure_4_12}
\end{figure}

The results were quite astonishing: first of all, we re-plotted the same green line of figure \ref{Figure_4_9}, indicating the previous values in the middle of the trajectory (frame 3000), where the protein was still vertical with respect to the POPC membrane, not exhibiting any variation. Second, we plotted a new black line, representing the results of distances residues-lipid surface in the last frame (6800). As we can see from the arrows (the arrows indicate the behaviour of the respective residues on the X axis during time), we note an opposite behaviour: residues that were attached previously, like THR187-LEU201 and GLN258-TRP261, now are evidently detached from the membrane. Contrary, residues that previously were collocated quite distant from the protein (e.g. 1-100, see green line), now are coming more in contact with the membrane. 

The inverted peaks between the green and black curve indicate that our protein totally exited from the membrane, meaning that has re-oriented in the space. In fact, we tried to make some VMD snapshots of what actually happened only in the last 80 [ns], and these are reported in the Appendix section, as figures in Appendix \ref{A_2}, \ref{A_3}, \ref{A_4}, \ref{A_5} (it is encouraged to read the descriptions of the figures, to understand better their sequential organization). These figures highlight a red zone, which is the one represented by the set of residues that was supposed to stay in contact with the membrane (THR187-LEU201 and GLN258-TRP261). It is clear that these membrane domains get out of the lipid surface, even in a relative short time: for 300 [ns] the protein just bends while staying attached, and only in the extended part (300-380 [ns]) completely exits, flattening on the upper leaflet. Moreover, from the black line it is possible to see that the distance of residues that at frame 3000 (circa 150 [ns]) were quite distant from the membrane decreases a lot, even below 1 [nm], entering in the range of possible supramolecular interactions with the membrane. Indeed, this could be an aspect that should be verified: it would be quite interesting to observe that the protein at the same time both exits the membrane and establishes new contacts with it. 

Overall, we can say that this behaviour is quite unclear. Even in the literature, we were not able to find material that confirmed our results. In the section of the Discussions, we will try to bring some connections with the biological relevance of our results. Moreover, since this part required us to wait for more 80 [ns] of simulations, we were quite stringent in terms of time; therefore, in the days between the deadline and the exam, we will try maybe to explore more this bending behaviour, in order to insert into the presentation some last-minute details, if possible. 