#!/bin/csh

set init = step5_input
set rest_prefix = step5_input
set mini_prefix = step6.0_minimization
set equi_prefix = step6.%d_equilibration
set prod_prefix = step7_production
set prod_step   = step7

# Equilibration
set cnt    = 0
set cntmax = 6

while ( ${cnt} <= ${cntmax} )
    @ pcnt = ${cnt} - 1
    set istep = `printf ${equi_prefix} ${cnt}`
    set pstep = `printf ${equi_prefix} ${pcnt}`
    if ( ${cnt} == 1 ) set pstep = ${mini_prefix}

    gmx_mpi grompp -f ${istep}.mdp -o ${istep} -c ${pstep}.gro -r ${rest_prefix}.gro -p topol.top -n index.ndx
    mpirun -np 5 mdrun_mpi -v -deffnm ${istep}
    @ cnt += 1
end



